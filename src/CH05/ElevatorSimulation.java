package CH05;

import java.util.Scanner;

public class ElevatorSimulation {
    public static void main(String[] agrs){
        Scanner in = new Scanner(System.in);

        System.out.print("Enter Floor #: ");
        int floor = in.nextInt();

        int actualFloor;

        if(floor < 13)
            actualFloor = floor;
        else
            actualFloor = floor + 1;

        System.out.printf("The elevator will travel to " +
                "the actual floor: %d ", actualFloor);
    }
}
