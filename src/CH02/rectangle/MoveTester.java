package CH02.rectangle;

import java.awt.Rectangle;

public class MoveTester {
   public static void main(String[] args){
       Rectangle box = new Rectangle(5, 10,20, 30);
       box.translate(15, 25);

       System.out.print("X: ");
       System.out.println(box.getX());
       System.out.println("Expected: 20");

       System.out.print("Y: ");
       System.out.println(box.getY());
       System.out.println("Expected: 35");
   }
}
/*
* Rectangle box = new Rectangle(5, 10, 20, 30);
        Rectangle box2= box;
        box.translate(15, 25);

        System.out.print("X: ");
        System.out.println(box2.getX());
        System.out.println("Expected: 20");

        System.out.print("Y: ");
        System.out.println(box2.getY());
        System.out.println("Expected: 35");
* */
