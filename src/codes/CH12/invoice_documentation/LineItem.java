package codes.CH12.invoice_documentation;

public class LineItem {

    /**
     * Computes the total cost of this line item.
     *
     * @return the total price
     */
    public double getTotalPrice() {
        return 0;
    }

    /**
     * Formats this item.
     *
     * @return a formatted string of this item
     */
    public String format() {
        return "";
    }


}
