package codes.CH12.invoice_documentation;

public class Product {


    /**
     Gets the product description.
     @return the description
     */
    public String getDescription() {
        return "";
    }


    /**
     Gets the product price.
     @return the unit price
     */
    public double getPrice(){
        return 0;
    }
}
