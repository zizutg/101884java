package codes.CH12.invoice_documentation;

public class Invoice {

    /**
     Adds a charge for a product to this invoice.
     @param aProduct the product that the customer ordered
     @param quantity the quantity of the product
     */
    public void add(Product aProduct, int quantity){
    }

    /**
     Formats the invoice.
     @return the formatted invoice
     */
    public String format(){
        return "";
    }
}
