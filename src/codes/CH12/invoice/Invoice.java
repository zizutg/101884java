package codes.CH12.invoice;
import java.text.NumberFormat;
import java.util.ArrayList;

/**
   Describes an invoice for a set of purchased products.
*/
public class Invoice
{     
   private Address billingAddress;
   private ArrayList<LineItem> items;

   /**
      Constructs an invoice.
      @param anAddress the billing address
   */
   public Invoice(Address anAddress)
   {  
      items = new ArrayList<LineItem>();
      billingAddress = anAddress;
   }
  
   /**
      Adds a charge for a product to this invoice.
      @param aProduct the product that the customer ordered
      @param quantity the quantity of the product
   */
   public void add(Product aProduct, int quantity)
   {  
      LineItem anItem = new LineItem(aProduct, quantity);
      items.add(anItem);
   }

   /**
      Formats the invoice.
      @return the formatted invoice
   */
   public String format()
   {
      final int DESCRIPTION_WIDTH = 30;
      final int PRICE_WIDTH = 8;
      final int QTY_WIDTH = 5;
      final int TOTAL_WIDTH = 8;
      
      String r =  "                     I N V O I C E\n\n"
            + billingAddress.format()
            + String.format("\n\n%-" + DESCRIPTION_WIDTH + "s%" + PRICE_WIDTH
            + "s%" + QTY_WIDTH + "s%" + TOTAL_WIDTH + "s\n",
            "Description", "Price", "Qty", "Total");
      
      int[] widths = new int[4];
      widths[0] = DESCRIPTION_WIDTH;
      widths[1] = PRICE_WIDTH;
      widths[2] = QTY_WIDTH;
      widths[3] = TOTAL_WIDTH;
   
      for (LineItem i : items)
         r = r + i.format() + "\n";

      r = r + String.format("\nAMOUNT DUE: $%" + TOTAL_WIDTH + "s", NumberFormat.getCurrencyInstance().format(getAmountDue()));

      return r;
   }

   /**
      Computes the total amount due.
      @return the amount due
   */
   public double getAmountDue()
   {  
      double amountDue = 0;
      for (LineItem i : items)
      {  
         amountDue = amountDue + i.getTotalPrice();
      }
      return amountDue;
   }
}
