package codes.CH06.random2;

public class NeedleTester {
    public static void main(String[] args) {
        Needle needle = new Needle();
        final int TRIES1 = 10000;
        final int TRIES2 = 1000000;

        for(int i = 1; i< TRIES1; i++)
            needle.drop();

        System.out.printf("Tries = %d, Hits = %d \n Tries/Hits = %8.5f \n",
                TRIES1, needle.getHits(),
                (double) needle.getTries() / needle.getHits());

        for(int i = 1; i< TRIES2; i++)
            needle.drop();

        System.out.printf("Tries = %d, Hits = %d \n Tries/Hits = %8.5f \n",
                TRIES2, needle.getHits(),
                (double) needle.getTries() / needle.getHits());
    }
}
