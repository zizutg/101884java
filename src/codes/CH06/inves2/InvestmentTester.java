package codes.CH06.inves2;

public class InvestmentTester {
    public static void main(String[] args){
        final double INIT_BALANCE = 10000;
        final double RATE = 5;
        final int YEARS = 20;

        Investment investment = new Investment(INIT_BALANCE, RATE);
        investment.waitYears(20);
        System.out.println("The balance after "+ YEARS + " is " +
                investment.getBalance() );
    }
}
