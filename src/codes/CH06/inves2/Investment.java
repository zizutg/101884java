package codes.CH06.inves2;

public

class Investment {
    private double balance, rate;
    private int years;

    public Investment(double aBanlance, double aRate){
        balance = aBanlance;
        rate = aRate;
        years = 0;
    }

    public void waitForYears(double targetBalance){
        while(balance < targetBalance){
            years++;
            double interest = balance * rate/100;
            balance += interest;
        }
    }
    public void waitYears(int numOfYears){
        for(int i = 1; i<=numOfYears;i++){
            double interest = balance * rate/100;
            balance += interest;
        }
        years =  numOfYears;
    }


    public double getBalance(){return  balance;}
    public int getYears(){return years;}
}
