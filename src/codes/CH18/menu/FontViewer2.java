package codes.CH18.menu;

import javax.swing.JFrame;

/**
   This program uses a menu to display font effects.
*/
public class FontViewer2
{  
   public static void main(String[] args)
   {  
      JFrame frame = new FontFrame2();
      JFrame frame1 = new FontFrame2();

      frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      frame.setTitle("FontViewer");
      frame.setVisible(true);

      frame1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      frame1.setTitle("FontViewer");
      frame1.setVisible(true);
   }
}

