package codes.CH11.chooser;

import javax.swing.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.Scanner;

public class FileChooser
{
   public static void main(String[] args) throws FileNotFoundException
   {
      JFileChooser chooser = new JFileChooser();
      String inputFileName = "", outputFileName = "";

      chooser.setDialogTitle("Input File");
      if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
         File selectedFile = chooser.getSelectedFile();
         inputFileName = selectedFile.getAbsolutePath();
         System.out.println(inputFileName);
      }

      chooser.setDialogTitle("Output File");
      if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
         File selectedFile = chooser.getSelectedFile();
         outputFileName = selectedFile.getAbsolutePath();
         System.out.println(outputFileName);
      }


      FileReader reader = new FileReader(inputFileName);
      Scanner in = new Scanner(reader);
      PrintWriter out = new PrintWriter(outputFileName);
      int lineNumber = 1;
      
      while (in.hasNextLine())
      {
         String line = in.nextLine();
         out.println("/* " + lineNumber + " */ " + line);
         lineNumber++;
      }

      out.close();
   }
}