package CH07.common;

import java.util.Scanner;

public class LargestInArray {
    public static void main(String[] args){
        final int LENGTH = 100;
        double[] values = new double[LENGTH];
        int currentSize = 0;

        System.out.println("Enter double values or Q to quit");
        Scanner in = new Scanner(System.in);
        while (in.hasNextDouble() && currentSize < values.length ){
            values[currentSize]  = in.nextDouble();
            currentSize++;
        }

        double smallestYet = values[0];

        for (int i = 1; i < currentSize; i++) {
            if (values[i] < smallestYet)
                smallestYet = values[i];
        }

        for (int i = 0; i < currentSize; i++) {
            System.out.print(values[i]);
            if (values[i] == smallestYet)
                System.out.print("<== This is smallest");
            System.out.println();
        }
    }
}
