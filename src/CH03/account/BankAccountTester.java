package CH03.account;

public class BankAccountTester {
    public static void main(String[] args){
        BankAccount kimsChecking = new BankAccount();
        kimsChecking.deposit(2000);
        kimsChecking.withdraw(500);

        System.out.println(kimsChecking.getBalance());
        System.out.println("Expected: 1500.0");

    }
}
