package CH03.account;

/**
 * A bank account has a balance that can be
 * changed by deposit or withdrawal
 * */
public class BankAccount {

    private double balance;

    /** To open bank account with zero balance*/
    public BankAccount(){
        balance = 0;
    }
    /** To open bank account with initial balance*/
    public BankAccount(double initialValue){
        balance = initialValue;
    }

    /** Deposits some amount of money to balance
     * @param amount the amount to be deposited*/
    public void deposit(double amount){
        this.balance += amount;
    }
    /** Withdraws some amount of money to balance
     * @param amount the amount to be withdrawn*/
    public void withdraw(double amount){
        this.balance -= amount;
    }
    /** Gets the current balance of the bank account.
     *  @return the current balance */
    public double getBalance(){ return balance;}
}
