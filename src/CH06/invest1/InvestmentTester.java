package CH06.invest1;

public class InvestmentTester {
    public static void main(String[] args){
        final double INIT_BALANCE = 10000;
        final double RATE = 5;

        double targetBalance = 3 * INIT_BALANCE;

        Investment investment = new Investment(INIT_BALANCE, RATE);
        investment.waitForYears(targetBalance);
        System.out.println("The investment triples after "+
                investment.getYears() + " years.");
    }
}
